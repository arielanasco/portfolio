import os
import argparse


parser = argparse.ArgumentParser()
 
# Adding optional argument
parser.add_argument("-a", "--admin", help = "Enter the email address of the adminstrator")
parser.add_argument("-i", "--ip", help = "Enter the Public IP address of the server")
parser.add_argument("-o", "--ouput", help = "Enter the filename.Default is the current directory")
parser.add_argument("-p", "--path", help = "Enter the path.If not specified, it will use the current directory as path")

# Read arguments from command line
args = parser.parse_args()
 
if args.admin and args.ip:
    directory_path = os.getcwd()
    if args.ouput:
        folder_name = args.ouput
    else:
        folder_name = os.path.basename(directory_path)
    if args.path:
        path = f"/{args.path}"
    else:
        path = "/"

    with open(f"{folder_name}.conf", 'w') as fp:
        fp.write(
f"""<VirtualHost *:80>
    ServerAdmin {args.admin}
    ServerName {args.ip}
    
    ErrorLog {directory_path}/logs/error.log
    CustomLog {directory_path}/logs/access.log combined

    Alias /static {directory_path}/static
    <Directory {directory_path}/static>
        Require all granted
    </Directory>

    Alias /media {directory_path}/media
    <Directory {directory_path}/media>
        Require all granted
    </Directory>

    <Directory {directory_path}/{folder_name}>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    WSGIDaemonProcess {folder_name} python-home=~/venv python-path={directory_path}/
    WSGIProcessGroup {folder_name}
    WSGIScriptAlias {path} {directory_path}/{folder_name}/wsgi.py
</VirtualHost>
""")
else:
    raise ("Error in arguments provided!")