from django.urls import path

from . import views

urlpatterns = [
    path('', views.profile, name='profile'),
    path('projects/', views.projects, name='projects'),
    path('skills/', views.skills, name='skills'),
    path('resume/', views.resume, name='resume'),
    path('download/', views.download, name='download')
]