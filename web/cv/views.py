from django.shortcuts import render
from .models import Gallery, Education, Tech, Project, UserProfile, CV, License

def profile(request):
    gallery = Gallery.objects.all()
    educations = Education.objects.all()
    stacks = Tech.objects.all()
    licenses = License.objects.all()
    user = UserProfile.objects.all().first()
    context ={
        'galleries':gallery,
        'educations':educations,
        'stacks':stacks,
        'profile':user,
        'licenses':licenses
    }
    return render (request=request, template_name="cv/profile.html", context=context)

def projects(request):
    projects = Project.objects.all()
    user = UserProfile.objects.all().first()

    context ={
        'projects':projects,
        'profile':user
    }
    return render (request=request, template_name="cv/projects.html", context=context)

def skills(request):
    user = UserProfile.objects.all().first()
    stacks = (Tech.objects
        .values('category','name','description','image','score',)
        .annotate()
        .order_by('category')
    )
    context ={
        'stacks':stacks,
        'profile':user
    }
    return render (request=request, template_name="cv/skills.html", context=context)

def resume(request):
    user = UserProfile.objects.all().first()
    cv = CV.objects.all().first()
    context ={
        'profile':user,
        'cv':cv
    }
    return render (request=request, template_name="cv/resume.html", context=context)

from django.conf import settings
from django.http import HttpResponse, Http404
from django.http import FileResponse


def download(request):
    cv = CV.objects.all().first()
    print(cv.file.path)
    return FileResponse(open(cv.file.path, 'rb'), as_attachment=True)

