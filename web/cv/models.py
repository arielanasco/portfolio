from django.db import models
from PIL import Image, ImageOps
import os
from uuid import uuid4
from django.db.models.signals import post_save
from django.contrib.auth.models import User

def profile_rename(instance, filename):
    upload_to = 'profile'
    ext = filename.split('.')[-1]
    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        filename = '{}.{}'.format(uuid4().hex, ext)
    return os.path.join(upload_to, filename)

class UserProfile(models.Model):  
    user = models.OneToOneField(User, on_delete=models.CASCADE)  
    image = models.ImageField(upload_to=profile_rename)
    intro = models.CharField(max_length=200)
    title = models.CharField(max_length=100)
    license = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = "User Profiles"

    def __str__(self):
        return f"{self.user.username}"

def path_and_rename(instance, filename):
    upload_to = 'photos'
    ext = filename.split('.')[-1]
    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        filename = '{}.{}'.format(uuid4().hex, ext)
    return os.path.join(upload_to, filename)

class Gallery(models.Model):
    description = models.CharField(max_length=100)
    image = models.ImageField(upload_to=path_and_rename)

    class Meta:
        verbose_name_plural = "Galleries"

    def __str__(self):
        return f"{self.image.url}"


def pre_process_logo(instance, filename):
    upload_to = 'logos'
    ext = filename.split('.')[-1]
    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        filename = '{}.{}'.format(uuid4().hex, ext)
    return os.path.join(upload_to, filename)

class Education(models.Model):
    LEVEL_CHOICES = [('PRIMARY', 'PRIMARY'),  ('SECONDARY','SECONDARY'), ('TERTIARY','TERTIARY'), ('TECHVOC', 'TECHVOC')]
    level = models.CharField(max_length=100, choices=LEVEL_CHOICES, default='PRIMARY')
    image = models.ImageField(upload_to=pre_process_logo, default='no-logo.jpg')
    school = models.CharField(max_length=200)
    description = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "Educations"

    def __str__(self):
        return f"{self.level}"

def pre_process_licenselogo(instance, filename):
    upload_to = 'license_logo'
    ext = filename.split('.')[-1]
    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        filename = '{}.{}'.format(uuid4().hex, ext)
    return os.path.join(upload_to, filename)

class License(models.Model):
    license = models.CharField(max_length=200)
    license_no = models.CharField(max_length=200)
    license_org = models.CharField(max_length=200)
    image = models.ImageField(upload_to=pre_process_licenselogo, default='no-logo.jpg')

    class Meta:
        verbose_name_plural = "Licenses"

    def __str__(self):
        return f"{self.license}"

def pre_process_icon(instance, filename):
    upload_to = 'stack-logo'
    ext = filename.split('.')[-1]
    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        filename = '{}.{}'.format(uuid4().hex, ext)
    return os.path.join(upload_to, filename)


class Tech(models.Model):
    category = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    image = models.ImageField(upload_to=pre_process_icon, default='no-logo.jpg')
    SCORE_CHOICES = zip( range(0,101), range(0,101) )
    score = models.IntegerField(choices=SCORE_CHOICES, default='0')

    class Meta:
        verbose_name_plural = "Techs"

    def __str__(self):
        return f"{self.name}"
    
def proj_rename(instance, filename):
    upload_to = 'project'
    ext = filename.split('.')[-1]
    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        filename = '{}.{}'.format(uuid4().hex, ext)
    return os.path.join(upload_to, filename)

class Project(models.Model):
    name = models.CharField(max_length=100)
    role = models.CharField(max_length=200)
    language = models.CharField(max_length=100)
    image = models.ImageField(upload_to=proj_rename, default='no-logo.jpg')
    url =  models.URLField(max_length=500, blank=True)

    class Meta:
        verbose_name_plural = "Projects"

    def __str__(self):
        return f"{self.name}"

def image_compressor(sender, **kwargs): 
    if kwargs["created"]:
        with Image.open(kwargs["instance"].image.path) as photo:
            photo = photo.convert("RGB")
            photo = ImageOps.fit(photo, (800, 800), Image.ANTIALIAS)
            photo.save(kwargs["instance"].image.path)

def image_thumbnail(sender, **kwargs): 
    if kwargs["created"]:
        with Image.open(kwargs["instance"].image.path) as photo:
            photo = photo.convert("RGB")
            photo = photo.resize((50,50))
            photo.save(kwargs["instance"].image.path)

class CV(models.Model):
    file = models.FileField(upload_to='cv') 

    class Meta:
        verbose_name_plural = "CVs"

    def __str__(self):
        return f"{self.file}"
    
post_save.connect(image_compressor, sender=Gallery)
post_save.connect(image_compressor, sender=Education)
post_save.connect(image_compressor, sender=License)
post_save.connect(image_thumbnail, sender=Tech)
post_save.connect(image_compressor, sender=Project)
