from django.contrib import admin
from .models import Gallery, Education, Tech, Project, UserProfile, CV, License

admin.site.register(Gallery)
admin.site.register(Education)
admin.site.register(Tech)
admin.site.register(Project)
admin.site.register(UserProfile)
admin.site.register(CV)
admin.site.register(License)
