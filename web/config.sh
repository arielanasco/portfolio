#!/bin/bash

initSetup=${1:-0}
projectDirectory=${2:-0}
adminEmail=${3:-"admin@email.here"}
domain=${4:-"domain-name.here"}

CURRENT=`pwd`
PROJECT=`basename "$CURRENT"`

if [ -e ~/build ]
then
    rsync -r $CURRENT ~
    cd ~/$PROJECT


    if [ $initSetup -eq 1 ]
    then
        #Software/System Update
        sudo apt-get -y update
        sudo apt-get -y install apt-utils
        sudo apt-get -y install python3-pip apache2 libapache2-mod-wsgi-py3 net-tools python3.8-venv python3-pip mysql-client libmysqlclient-dev rsync
        sudo apt-get -y upgradels

    fi

    #Install necessary pip packages
    python3 -m venv ~/venv
    source ~/venv/bin/activate
    python --version
    mysql --version
    which python
    pip install -r requirements.txt
    pip list

    #Run Django related commands
    # python manage.py makemigrations
    # python manage.py migrate
    # python manage.py collectstatic --noinput
    # python manage.py check

    if [ $initSetup -eq 1 ]
    then
        python config.py -a "$adminEmail" -i "$domain"
        echo "$PROJECT.conf has been created"
        sudo cp "$PROJECT.conf" /etc/apache2/sites-available/
        sudo a2ensite  "$PROJECT.conf"
        sudo ufw allow 'Apache Full'
        mkdir logs
    fi

    sudo chown :www-data .
    chmod +x ~
    sudo apache2ctl configtest
    sudo systemctl restart apache2
    rm -rf ~/build
else
    echo "Build directory is empty!"
fi

