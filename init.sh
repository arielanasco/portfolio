#!/bin/bash

#Software/System Update
sudo apt-get -y update
sudo apt-get -y install apt-utils
sudo apt-get -y install python3-pip apache2 libapache2-mod-wsgi-py3 net-tools python3.8-venv python3-pip mysql-client libmysqlclient-dev
sudo apt-get -y upgrade

#Install necessary pip packages
python3 -m venv venv
source venv/bin/activate
python --version
mysql --version
which python
pip install -r /builds/arielanasco/portfolio/requirements.txt
pip list

#Run Django related commands
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --noinput
python manage.py check

python create_virtual_host.py -a ariel@linkmind.co.jp -i gingineer.online


CURRENT=`pwd`
BASENAME=`basename "$CURRENT"`
echo "$BASENAME.conf has been created"
sudo cp "$BASENAME.conf" /etc/apache2/sites-available/
sudo a2ensite  "$BASENAME.conf"


mkdir logs
sudo chown :www-data $CURRENT
chmod +x ~
sudo ufw allow 'Apache Full'
sudo apache2ctl configtest
sudo systemctl restart apache2



